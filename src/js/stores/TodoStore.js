import { EventEmitter } from "events";

import dispatcher from "../dispatcher";
import axios from "axios/index";

class TodoStore extends EventEmitter {
  constructor() {
    super()
      this.todos = [];
      axios.get("http://localhost:3000/api/1.0.0/todos")
          .then(function (response) {
              dispatcher.dispatch({type :"RECEIVE_TODOS", todos: response.data});
          });
  }

  createTodo(text) {
    let callAPI = {id: 104, complete: true};
    var classe=this;
    axios.post("http://localhost:3000/api/1.0.0/create/", {
        libelle: text
      })
      .then(function (response) {
        console.log(response);
        classe.todos = response;
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  updateTodo(id, libelle) {
    let callAPI = {id: 104, complete: true};

    axios.post("http://localhost:3000/api/1.0.0/update/"+id, {
      libelle: libelle
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  updateDoneTodo(id) {
    let callAPI = {id: 104, complete: true};

    axios.post("http://localhost:3000/api/1.0.0/update/todo/"+id)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  getAll() {
    return this.todos;
  }

  handleActions(action) {
    switch(action.type) {
      case "CREATE_TODO": {
        this.createTodo(action.text);
        break;
      }
      case "RECEIVE_TODOS": {
        this.todos = action.todos;
        this.emit("change");
        break;
      }
    }
  }

}

const todoStore = new TodoStore;
dispatcher.register(todoStore.handleActions.bind(todoStore));

export default todoStore;
