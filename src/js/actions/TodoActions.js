import dispatcher from "../dispatcher";

import axios from 'axios';

export function createTodo(text) {
  let callAPI = {id: 104, complete: true};

  axios.post("http://localhost:3000/api/1.0.0/create/", {
      libelle: text
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
}

export function deleteTodo(id) {
  let callAPI = {id: 104, complete: true};

  axios.post("http://localhost:3000/api/1.0.0/delete/"+id)
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
}

export function reloadTodos() {
  // axios("http://someurl.com/somedataendpoint").then((data) => {
  //   console.log("got the data!", data);
  // })
  //   this.todos = [];
  //   axios.get("http://localhost:3000/api/1.0.0/todos")
  //       .then(function (response) {
  //         dispatcher.dispatch({type :"RECEIVE_TODOS", todos: response.data});
  //           console.log(response.data._id);
  //       });

  // dispatcher.dispatch({type: "FETCH_TODOS"});
  // setTimeout(() => {
  //   dispatcher.dispatch({type: "RECEIVE_TODOS", todos: [
  //     {
  //       id: res.data.id ,
  //       libelle: res.data.libelle
  //
  //     },

  //   ]});
  // }, 1000);
}
