import React from "react";

import Todo from "../components/Todo";
import * as TodoActions from "../actions/TodoActions";
import TodoStore from "../stores/TodoStore";


export default class Todos extends React.Component {
  constructor() {
    super();
    this.getTodos = this.getTodos.bind(this);
    this.deleteTodos = this.deleteTodos.bind(this);
    this.createTodos = this.createTodos.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      todos: TodoStore.getAll(),
      value: ''
    };
  }

  componentWillMount() {
    TodoStore.on("change", this.getTodos);
  }

  componentWillUnmount() {
    TodoStore.removeListener("change", this.getTodos);
  }

  getTodos() {
    this.setState({
      todos: TodoStore.getAll(),
    });
  }

  reloadTodos() {
    TodoActions.reloadTodos();
  }
  
  deleteTodos(index, id) {
    this.setState({
      todos: this.state.todos.filter((x,i) => i != index)
    });
    TodoActions.deleteTodo(id);
  }

  onKeyPress(event) {
    if (event.key == "Enter") {
      var index = event.target.attributes.getNamedItem('data-index').value;
      TodoStore.updateTodo(this.state.todos[index]._id, event.target.value)
    }
  }

  updateDoneTodos(id) {
    TodoStore.updateDoneTodo(id);
    window.location.reload();
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleChangeLibelle(event) {
    var index = event.target.attributes.getNamedItem('data-index').value;
    this.state.todos[index].libelle = event.target.value;
    this.setState({todos: this.state.todos});
  }

  createTodos(event) {
    TodoStore.createTodo(this.state.value);
    // TodoActions.reloadTodos();
    /*console.log(TodoStore.getAll());
      this.setState({
        todos: TodoStore.getAll()
      });*/
      window.location.reload();
  }

  render() {
    const { todos } = this.state;
    
    const TodoComponents = todos.map((todo, index) => {
        return <li class="todo" key={index}>
        <input class="libelle" value={todo.libelle} onChange={this.handleChangeLibelle.bind(this)} data-index={index} onKeyDown={this.onKeyPress.bind(this)} />
        <span>
          <a href="#" class="check" onClick={this.updateDoneTodos.bind(this, todo._id)}><i class={todo.done ? "fa fa-check fa-lg vert" : "fa fa-check fa-lg" }></i></a>
          <a href="#" class="delete-todo" onClick={this.deleteTodos.bind(this, index, todo._id)}><i class="fa fa-trash-o fa-lg"></i></a>
        </span>
        </li>;
    });

    return (
      <div>
        <h1>To-Do List</h1>
        <div class="container">
          <div class="row">
              <input type="text" class="col-xs-8 col-sm-9" value={this.state.value} onChange={this.handleChange} />
              <div class="input-buttons col-xs-4 col-sm-3">
                <a class="clear-text pointer"><i class="fa fa-trash-o fa-2x"></i></a>
                <a onClick={this.createTodos.bind(this)} class="add-todo pointer"><i class="fa fa-plus fa-2x"></i></a>
              </div>
          </div>
          <div class="row">
            <div class="error">
              <a href="#"><i class="fa fa-times"></i></a>
              <p>We both know you should be doing something right now. Please enter a task.</p>
            </div>
            {/* let activeState = toDo.active ? 'active' : 'inactive',
                 classes = "message " + activeState;

             return <div className={classes} key={index} onClick={this.setActiveState.bind(this, index)}>

                {<span className="close" onClick={this.removeToDo.bind(this, index)}>X</span>}
             </div> */}
            <ul class="todo-list col-xs-12">
              {TodoComponents}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
